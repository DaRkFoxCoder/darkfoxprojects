<?php
function getDateDiff($datePast) {
	$date = new DateTime($datePast);
    $now = new DateTime();
    return $date->diff($now)->format("%d days, %h hours and %i minuts");
}
?>