<?php
session_start();
require_once($_SESSION['BASE_PATH']."/model/autoload.php");
$projectsPage = unserialize($_SESSION['projectsPage']);
$status['logged'] = false;
if (isset($_POST['uname']) && isset($_POST['upassword'])) {
	$uname = $_POST['uname'];
	$upassword = $_POST['upassword'];
	$login = $projectsPage->login($uname,$upassword);
	if($login['logged']) {
		$status['logged'] = true;
	} else {
		$status['invalid'] = $login['invalid'];
		$status['logged'] = false;
	}
}
echo json_encode($status);
?>