<?php
session_start();
/**
 * Require of Global Config
 */
require_once ("../../config/config.inc.php");
/**
 * Require of DB Config
 */
require_once ("../../config/db.inc.php");
/**
 * Require of autoload for model classes
 */
require_once ("../../model/autoload.php");
if (! isset($_SESSION['projectsPage'])) {
    $projectsPage = new projectsPage("DaRkFoxProjects");
    $projectsPage->populate();
    $_SESSION['projectsPage'] = serialize($projectsPage);
} else {
    $projectsPage = unserialize($_SESSION['projectsPage']);
}
?>