<?php
session_start();
require_once($_SESSION['BASE_PATH']."/model/autoload.php");
$projectsPage = unserialize($_SESSION['projectsPage']);
$commentid = $_POST['commentid'];
$userid = $_POST['userid'];
switch($_POST['type']) {
	case "dislike": {
		echo $projectsPage->valorateComment($commentid, $userid, 0);
		break;
	}
	case "like": {
		echo $projectsPage->valorateComment($commentid, $userid, 1);
		break;
	}
}
?>