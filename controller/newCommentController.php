<?php
session_start();
require_once($_SESSION['BASE_PATH']."/model/autoload.php");
$projectsPage = unserialize($_SESSION['projectsPage']);
$comment = strip_tags($_POST['comment']);
$userid = $_POST['userid'];
$projectid = $_POST['projectid'];
if (isset($_POST['commentidref'])) {
	$commentref = $_POST['commentidref'];
}
else {
	$commentref = null;
}
echo $projectsPage->comment($comment, $userid, $projectid, $commentref);
?>