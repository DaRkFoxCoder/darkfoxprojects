<?php
	
	require_once($_SESSION['BASE_PATH']."/model/autoload.php");

	$GLOBALS['SERVER'] = "localhost";
	$GLOBALS['USERNAME'] = "root";
	$GLOBALS['PASSWORD'] = "";
	$GLOBALS['DATABASE'] = "darkfoxprojects";
	$GLOBALS['DATABASETYPE'] = "mysql";

	if (!isset($_SESSION['dbconnection'])) {
		$db = new db();
		$_SESSION['db'] = serialize($db);
	}
?>