-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-06-2016 a las 21:21:54
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `darkfoxprojects`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `ID` bigint(20) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`ID`, `Name`) VALUES
(1, 'Conquer Online');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects`
--

CREATE TABLE `projects` (
  `ID` bigint(20) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `LastUpdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Link` varchar(100) DEFAULT '#',
  `Price` decimal(10,2) DEFAULT '0.00',
  `Score` decimal(10,2) DEFAULT '0.00',
  `Category` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `projects`
--

INSERT INTO `projects` (`ID`, `Name`, `Description`, `LastUpdate`, `Link`, `Price`, `Score`, `Category`) VALUES
(1, 'ConquerMonsterTranslator', 'Translate Monster File of Conquer Client', '2015-05-28 00:00:00', 'https://mega.nz/#F!0JIQyZqZ!6x208E0_ALM1k_MpRDoUKw', '0.00', '3.50', 1),
(2, 'ConquerSourceConfigurator', 'Configure your Source of Conquer Online', '2015-04-06 00:00:00', 'https://mega.nz/#F!VVw1FBRY!H1OIFKbVZEh0vWmuuBrC_w', '0.00', '4.00', 1),
(3, 'OpenConquerLoader', 'Loader for Conquer Online 2.0', '2015-06-30 00:00:00', 'https://mega.nz/#F!sApGDD7I!UpvKANBMIJVyd7bzz3Joiw', '1.00', '4.50', 1),
(4, 'ConquerPriceEditor', 'Edit your items price', '2015-04-03 00:00:00', 'https://mega.nz/#F!EJhQVIyb!6DfI26zriV0VQa37q3l_UA', '0.00', '5.00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_comments`
--

CREATE TABLE `project_comments` (
  `ID_Comment` bigint(20) NOT NULL,
  `ID_Project` bigint(20) NOT NULL,
  `ID_User` bigint(20) NOT NULL,
  `Comment` varchar(200) NOT NULL,
  `Likes` int(11) NOT NULL,
  `Dislikes` int(11) NOT NULL,
  `ID_Comment_Reference` bigint(20) DEFAULT NULL,
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_comment_valorations`
--

CREATE TABLE `project_comment_valorations` (
  `ID` bigint(20) NOT NULL,
  `ID_Comment` bigint(20) NOT NULL,
  `ID_User` bigint(20) NOT NULL,
  `LikeDislike` tinyint(1) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_valorations`
--

CREATE TABLE `project_valorations` (
  `ID_Valoration` bigint(20) NOT NULL,
  `ID_Project` bigint(20) NOT NULL,
  `ID_User` bigint(20) NOT NULL,
  `Valoration` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `project_valorations`
--

INSERT INTO `project_valorations` (`ID_Valoration`, `ID_Project`, `ID_User`, `Valoration`) VALUES
(1, 3, 1, '4.50'),
(2, 1, 1, '3.00'),
(3, 2, 1, '4.00'),
(4, 4, 1, '5.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `ID` bigint(20) NOT NULL,
  `Username` varchar(60) NOT NULL,
  `Password` varchar(60) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Website` varchar(60) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`ID`, `Username`, `Password`, `Email`, `Website`, `Status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'cristian14rap@gmail.com', 'http://www.darkfoxprojects.com', 1),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'google@google.com', 'http://www.google.es', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`ID`,`Category`),
  ADD KEY `fk_Category` (`Category`),
  ADD KEY `ID` (`ID`);

--
-- Indices de la tabla `project_comments`
--
ALTER TABLE `project_comments`
  ADD PRIMARY KEY (`ID_Comment`),
  ADD KEY `ID_Project` (`ID_Project`),
  ADD KEY `ID_User` (`ID_User`),
  ADD KEY `ID_Comment_Reference` (`ID_Comment_Reference`);

--
-- Indices de la tabla `project_comment_valorations`
--
ALTER TABLE `project_comment_valorations`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_Comment` (`ID_Comment`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indices de la tabla `project_valorations`
--
ALTER TABLE `project_valorations`
  ADD PRIMARY KEY (`ID_Valoration`),
  ADD KEY `ID_Project` (`ID_Project`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `projects`
--
ALTER TABLE `projects`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `project_comments`
--
ALTER TABLE `project_comments`
  MODIFY `ID_Comment` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `project_comment_valorations`
--
ALTER TABLE `project_comment_valorations`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `project_valorations`
--
ALTER TABLE `project_valorations`
  MODIFY `ID_Valoration` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `fk_Category` FOREIGN KEY (`Category`) REFERENCES `categories` (`ID`);

--
-- Filtros para la tabla `project_comments`
--
ALTER TABLE `project_comments`
  ADD CONSTRAINT `fk_ID_Comment_Reference_Comment` FOREIGN KEY (`ID_Comment_Reference`) REFERENCES `project_comments` (`ID_Comment`),
  ADD CONSTRAINT `fk_ID_Project_Comment` FOREIGN KEY (`ID_Project`) REFERENCES `projects` (`ID`),
  ADD CONSTRAINT `fk_ID_User_Comment` FOREIGN KEY (`ID_User`) REFERENCES `users` (`ID`);

--
-- Filtros para la tabla `project_comment_valorations`
--
ALTER TABLE `project_comment_valorations`
  ADD CONSTRAINT `fk_ID_Comment_ValorationC` FOREIGN KEY (`ID_Comment`) REFERENCES `project_comments` (`ID_Comment`),
  ADD CONSTRAINT `fk_ID_User_ValorationC` FOREIGN KEY (`ID_User`) REFERENCES `users` (`ID`);

--
-- Filtros para la tabla `project_valorations`
--
ALTER TABLE `project_valorations`
  ADD CONSTRAINT `fk_ID_Project` FOREIGN KEY (`ID_Project`) REFERENCES `projects` (`ID`),
  ADD CONSTRAINT `fk_ID_User` FOREIGN KEY (`ID_User`) REFERENCES `users` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
