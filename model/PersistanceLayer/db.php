<?php
require_once($_SESSION['BASE_PATH']."/config/db.inc.php");

class db {
	private $dsn, $username, $password, $link;

	public function __construct() {
		$this->setDSN($GLOBALS['DATABASETYPE'].":dbname=".$GLOBALS['DATABASE'].";host=".$GLOBALS['SERVER'].";charset=utf8");
		$this->setUsername($GLOBALS['USERNAME']);
		$this->setPassword($GLOBALS['PASSWORD']);
		$this->connect();
	}

	public function getDSN() {
		return $this->dsn;
	}

	public function setDSN($value) {
		$this->dsn = $value;
	}
	
	public function getUsername() {
		return $this->username;
	}

	public function setUsername($value) {
		$this->username = $value;
	}
	
	public function getPassword() {
		return $this->password;
	}

	public function setPassword($value) {
		$this->password = $value;
	}

	private function connect() {
		try {
			$this->link = new PDO($this->dsn, $this->username, $this->password);
		} catch (PDOException $e) {
			print "�Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}

	public function __sleep() {
		return array('dsn', 'username', 'password');
	}

	public function __wakeup() {
		$this->connect();
	}

	public function getLink() {
		return $this->link;
	}

	//functions to execute in model dao db...
	public function getProjects() {
		$sql = "SELECT P.ID, P.Name, P.Description, P.LastUpdate, P.Link, P.Price, P.Score, P.Category, P.Hot FROM projects P ORDER BY P.LastUpdate DESC";
		$sqlQuery = $this->getLink()->prepare($sql);
		$sqlQuery->execute();
		$result = $sqlQuery->FetchAll();
		return $result;
	}

	public function getCategories() {
		$sql = "SELECT C.ID, C.Name FROM categories C ORDER BY C.Name ASC";
		$sqlQuery = $this->getLink()->prepare($sql);
		$sqlQuery->execute();
		$result = $sqlQuery->FetchAll();
		return $result;
	}
	
	public function getCommentsProject($idproject) {
		$sql = "SELECT PC.ID_Comment, PC.ID_User, PC.Comment, PC.Likes, PC.Dislikes, PC.ID_Comment_Reference, PC.Date FROM project_comments PC WHERE PC.ID_Project='".$idproject."' AND PC.ID_Comment_Reference IS NULL ORDER BY PC.Likes, PC.Date DESC, PC.ID_Comment ASC";
		$sqlQuery = $this->getLink()->prepare($sql);
		$sqlQuery->execute();
		$result = $sqlQuery->FetchAll();
		return $result;
	}

	public function getCommentsWithRef($idcommentref) {
		$sql = "SELECT PC.ID_Comment, PC.ID_User, PC.Comment, PC.Likes, PC.Dislikes, PC.ID_Comment_Reference, PC.Date FROM project_comments PC WHERE PC.ID_Comment_Reference='".$idcommentref."' ORDER BY PC.Likes, PC.Date DESC, PC.ID_Comment ASC";
		$sqlQuery = $this->getLink()->prepare($sql);
		$sqlQuery->execute();
		$result = $sqlQuery->FetchAll();
		return $result;
	}

	public function login($uname, $upassword) {
		$status['logged'] = false;
		$sql = "SELECT U.ID, U.Username, U.Password, U.Status FROM users U WHERE U.Username=:uname";
		$sqlQuery = $this->getLink()->prepare($sql);
		$sqlQuery->execute(array(':uname'=>$uname));
		$result = $sqlQuery->fetch(PDO::FETCH_ASSOC);
		if ($result && $sqlQuery->rowCount()>0) {
			$sql = "SELECT U.ID, U.Username, U.Password, U.Status FROM users U WHERE U.Username=:uname AND U.Password=:upassword";
			$sqlQuery = $this->getLink()->prepare($sql);
			$sqlQuery->execute(array(':uname'=>$uname, ':upassword'=>md5($upassword)));
			$result = $sqlQuery->fetch(PDO::FETCH_ASSOC);
			if ($sqlQuery->rowCount()>0) {
				$_SESSION['user_session'] = $result['ID'];
				$status['logged'] = true;
			} else {
				$status['invalid'] = "password";
			}
		} else {
				$status['invalid'] = "username";
		}
		return $status;
	}

	public function rate($idproject, $userid, $rate) {
		$status['rate'] = false;
		if (!empty($userid) && $userid >= 1) {
			$sql = "SELECT * FROM project_valorations WHERE ID_Project=$idproject AND ID_User=$userid";
			$sqlQuery = $this->getLink()->prepare($sql);
			if ($sqlQuery->execute()) {
				$result = $sqlQuery->fetch(PDO::FETCH_ASSOC);
				if ($sqlQuery->rowCount()<=0) {
					$sql = "INSERT INTO project_valorations VALUES('', '$idproject', '$userid', '$rate')";
					$sqlQuery = $this->getLink()->prepare($sql);
					if ($sqlQuery->execute()) {
						$status['rate'] = true;
						//recalculate and update score of each project
						$sql = "SELECT SUM(PV.Valoration)/COUNT(*) as score FROM project_valorations PV WHERE ID_Project=$idproject";
						$sqlQuery = $this->getLink()->prepare($sql);
						if ($sqlQuery->execute()) {
							 $result = $sqlQuery->fetch(PDO::FETCH_ASSOC);
							 $score = $result['score'];
							 $sql = "UPDATE projects SET Score='$score' WHERE ID='$idproject'";
							 $sqlQuery = $this->getLink()->prepare($sql);
							 $sqlQuery->execute();
						}
						//
					} else {
						$status['msg'] = "QUERY_ERROR";
					}
				} else {
					$status['msg'] = "ALREADY_VOTED";
				}
			} else {
					$status['msg'] = "QUERY_ERROR";
			}
		} else {
			$status['msg'] = "NO_LOGGED";
		}
		echo json_encode($status);
	}
	
	public function comment($comment, $userid, $projectid, $commentref) {
		if ($commentref == null) {
			$commentref = null;
		}
		$status['comment'] = false;
		if (!empty($userid) && $userid >= 1) {
			$sql = "INSERT INTO project_comments VALUES('', :projectid, :userid, :comment, '0', '0', :commentref, CURRENT_TIMESTAMP)";
			$sqlQuery = $this->getLink()->prepare($sql);
			$sqlQuery->bindParam(":projectid", $projectid);
			$sqlQuery->bindParam(":userid", $userid);
			$sqlQuery->bindParam(":comment", $comment);
			$sqlQuery->bindParam(":commentref", $commentref);
			if ($sqlQuery->execute()) {
				$status['comment'] = true;
			} else {
				$status['msg'] = "QUERY_ERROR";
			}
		} else {
			$status['msg'] = "NO_LOGGED";
		}
		echo json_encode($status);
	}

	public function updateValorationsComment($commentid, $likedislike) {
		$updated = false;
		$sql = "SELECT COUNT(*) as valorationCount FROM project_comment_valorations WHERE ID_Comment='$commentid' AND LikeDislike='$likedislike'";
		$sqlQuery = $this->getLink()->prepare($sql);
		if ($sqlQuery->execute()) {
			$result = $sqlQuery->fetch(PDO::FETCH_ASSOC);
			$setVal = "Likes";
			if ($likedislike == 0) {
				$setVal = "Dislikes";
			}
			$sql = "UPDATE project_comments SET ".$setVal."='".$result['valorationCount']."' WHERE ID_Comment='$commentid'";
			$sqlQuery = $this->getLink()->prepare($sql);
			if ($sqlQuery->execute()) {
				$updated = true;
			}
		}
		return $updated;
	}

	public function valorateComment($commentid, $userid, $valoration) {
		$status['status'] = "fail";
		if (!empty($userid) && $userid >= 1) {
			$sql = "SELECT COUNT(*) as valorationCount, LikeDislike, ID FROM project_comment_valorations WHERE ID_Comment='$commentid' AND ID_User='$userid'";
			$sqlQuery = $this->getLink()->prepare($sql);
			if ($sqlQuery->execute()) {
				$result = $sqlQuery->fetch(PDO::FETCH_ASSOC);
				$valCount = (int)($result['valorationCount']);
				if ($valCount == 0) {
					$sql = "INSERT INTO project_comment_valorations VALUES('', '$commentid', '$userid', '$valoration')";
					$sqlQuery = $this->getLink()->prepare($sql);
					if ($sqlQuery->execute()) {
						$status['status'] = "valorated";
					}
				} else if ($valCount == 1) {
					if ($valoration != $result['LikeDislike']) {
						$sql = "UPDATE project_comment_valorations SET LikeDislike='".$valoration."' WHERE ID='".$result['ID']."'";
						$sqlQuery = $this->getLink()->prepare($sql);
						if($sqlQuery->execute()) {
							$status['status'] = "valorated";
						} else {
							$status['msg'] = "QUERY_ERROR";
						}
					} else {
						$sql = "DELETE FROM project_comment_valorations WHERE ID='".$result['ID']."'";
						$sqlQuery = $this->getLink()->prepare($sql);
						if($sqlQuery->execute()) {
							$status['status'] = "unvalorated";
						} else {
							$status['msg'] = "QUERY_ERROR";
						}
					}
				}
			} else {
				$status['msg'] = "QUERY_ERROR";
			}
		} else {
			$status['msg'] = "NO_LOGGED";
		}
		if($this->updateValorationsComment($commentid, 0)) {
			if($this->updateValorationsComment($commentid, 1)) {
				echo json_encode($status);
			} else {
				$status['msg'] = "QUERY_ERROR";
			}
		} else {
			$status['msg'] = "QUERY_ERROR";
		}
	}

	public function getUserDataByID($userid) {
		$userData = null;
		if (!empty($userid) && $userid >= 1) {
			$sql = "SELECT * FROM users WHERE ID=$userid";
			$sqlQuery = $this->getLink()->prepare($sql);
			if ($sqlQuery->execute()) {
				$result = $sqlQuery->fetch(PDO::FETCH_ASSOC);
				if ($sqlQuery->rowCount()>0) {
					$userData = $result;
				}
			}
		}
		return $userData;
	}

	public function getCommentDataByIDRef($commentid) {
		$commentData = null;
		if (!empty($commentid) && $commentid >= 1) {
			$sql = "SELECT * FROM project_comments WHERE ID_Comment=$commentid";
			$sqlQuery = $this->getLink()->prepare($sql);
			if ($sqlQuery->execute()) {
				$result = $sqlQuery->fetch(PDO::FETCH_ASSOC);
				if ($sqlQuery->rowCount()>0) {
					$commentData = $result;
				}
			}
		}
		return $commentData;
	}
}
?>