<?php 
class User {
    private $id;
    private $username;
    private $status;
    private $email;
    private $website;
    private $profile;

    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param field_type $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return the name
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param name
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return the status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return the email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
	
	/**
     * @return the website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }
	
	/**
     * @return the profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    function __construct() {
    }
}
?>