<?php 
class Comment {
    private $id;
    private $user;
    private $comment;
    private $likes;
    private $dislikes;
    private $commentReference;
    private $date;

    /**
     * @return the id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

	/**
     * @return the user
    */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
	
    /**
     * @return the comment
     */
    public function getComment()
    {
        return $this->comment;
    }
	
	/**
     * @param comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
	
	/**
     * @return the likes
     */
    public function getLikes()
    {
        return $this->likes;
    }
	
	/**
     * @param likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }
	
	/**
     * @return the dislikes
     */
    public function getDislikes()
    {
        return $this->dislikes;
    }
	
	/**
     * @param dislikes
     */
    public function setDislikes($dislikes)
    {
        $this->dislikes = $dislikes;
    }
	
	/**
     * @return the commentReference
    */
    public function getCommentReference()
    {
        return $this->commentReference;
    }

    /**
     * @param commentReference
     */
    public function setCommentReference($commentReference)
    {
        $this->commentReference = $commentReference;
    }
	
	/**
     * @return the date
     */
    public function getDate()
    {
        return $this->date;
    }
	
	/**
     * @param date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    function __construct() {
    }
}
?>