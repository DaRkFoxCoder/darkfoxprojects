<?php 
class Category {
    private $id;
    private $name;

    /**
     * @return the id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return the name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    function __construct() {
    }
}
?>