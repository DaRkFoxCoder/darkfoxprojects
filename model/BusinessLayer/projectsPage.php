<?php
class projectsPage {
    private $name;
    private $projects = null;
    private $categories = null;
    
    /**
     * @return the name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name of projects page
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return the projects
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @param projects
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;
    }

    /**
     * @return the categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    function __construct($name) {
        $this->setName($name);
    }
    
    function populate() {
        $db = unserialize($_SESSION['db']);
        //start - populate projects
        $projects = $db->getProjects();
        $projectObjects = array();
        foreach ($projects as $project) {
            $projectObj = new Project();
            $projectObj->setId($project['ID']);
            $projectObj->setName($project['Name']);
            $projectObj->setDescription($project['Description']);
            $projectObj->setLastUpdate($project['LastUpdate']);
            $projectObj->setLink($project['Link']);
            $projectObj->setPrice($project['Price']);
            $projectObj->setScore($project['Score']);
            $projectObj->setCategory($project['Category']);
            $projectObj->setHot($project['Hot']);
			//start - populate comments of each project
			$comments = $db->getCommentsProject($project['ID']);
			$commentObjects = array();
			foreach ($comments as $comment) {
				$commentObj = new Comment();
				$commentObj->setId($comment['ID_Comment']);
				$commentObj->setComment($comment['Comment']);
                $commentObj->setDate($comment['Date']);
                //start - set user, user reference of comment
                $userObj = $this->getUserByID($comment['ID_User']);
                $commentObj->setUser($userObj);
                if ($comment['ID_Comment_Reference']==null) {
                    $commentObj->setCommentReference(null);
                } else {
                    $commentObjRef = $this->getCommentByID($comment['ID_Comment_Reference']);
                    $commentObj->setCommentReference($commentObjRef);
                }
                //end - set user, user reference of comment
				$commentObj->setLikes($comment['Likes']);
				$commentObj->setDislikes($comment['Dislikes']);
            	$commentObjects[] = $commentObj;
			}
			$projectObj->setComments($commentObjects);
			//end - populate comments of each project
            $projectObjects[] = $projectObj;
        	//end - populate projects
        }
        $this->setProjects($projectObjects);
        //start - populate categories
        $categories = $db->getCategories();
        $categoryObjects = array();
        foreach ($categories as $category) {
            $categoryObj = new Category();
            $categoryObj->setId($category['ID']);
            $categoryObj->setName($category['Name']);
            $categoryObjects[] = $categoryObj;
        }
        $this->setCategories($categoryObjects);
        //end - populate categories
    }

    function getProject($id) {
        $projectObj = null;
        $projects = $this->getProjects();
        foreach ($projects as $project) {
            if ($project->getId()==$id) {
                $projectObj = $project;
            }
        }
        return $projectObj;
    }

    function getProjectsWithCat($cat, $exclude_id) {
        $projectsObj = array();
        $projects = $this->getProjects();
        foreach ($projects as $project) {
            if ($project->getCategory()==$cat && $project->getId()!=$exclude_id) {
                $projectsObj[] = $project;
            }
        }
        return $projectsObj;
    }

    function getHotProjects() {
        $projectsObj = array();
        $projects = $this->getProjects();
        foreach ($projects as $project) {
            if ($project->getHot()==1) {
                $projectsObj[] = $project;
            }
        }
        return $projectsObj;
    }

    function getCategory($id) {
        $categoryObj = null;
        $categories = $this->getCategories();
        foreach ($categories as $category) {
            if ($category->getId()==$id) {
                $categoryObj = $category;
            }
        }
        return $categoryObj;
    }

    function login($uname, $upassword) {
        $db = unserialize($_SESSION['db']);
        return $db->login($uname, $upassword);
    }

    function rate($idproject, $userid, $rate) {
        $db = unserialize($_SESSION['db']);
        return $db->rate($idproject, $userid, $rate);
    }
	
	function comment($comment, $userid, $projectid, $commentref) {
		$db = unserialize($_SESSION['db']);
        return $db->comment($comment, $userid, $projectid, $commentref);
	}

    function valorateComment($commentid, $userid, $valoration) {
        $db = unserialize($_SESSION['db']);
        return $db->valorateComment($commentid, $userid, $valoration);
    }

    function getUserByID($userid) {
        $user = null;
        $db = unserialize($_SESSION['db']);
        $userdata = $db->getUserDataByID($userid);
        if ($userdata!=null) {
            $user = new User();
            $user->setId($userdata['ID']);
            $user->setUsername($userdata['Username']);
            $user->setStatus($userdata['Status']);
            $user->setEmail($userdata['Email']);
            $user->setWebsite($userdata['Website']);
        }
        return $user;
    }

    function getCommentByID($commentid) {
        $comment = null;
        $db = unserialize($_SESSION['db']);
        $commentData = $db->getCommentDataByIDRef($commentid);
        if ($commentData!=null) {
            $comment = new Comment();
            $comment->setId($commentData['ID_Comment']);
            $comment->setComment($commentData['Comment']);
            $comment->setDate($commentData['Date']);
            //start - set user, user reference of comment
            $userObj = $this->getUserByID($commentData['ID_User']);
            $comment->setUser($userObj);
            if ($commentData['ID_Comment_Reference']==null) {
                $comment->setCommentReference(null);
            } else {
                $comment = $this->getCommentByID($commentData['ID_Comment_Reference']);
                $comment->setCommentReference($comment);
            }
            //end - set user, user reference of comment
            $comment->setLikes($commentData['Likes']);
            $comment->setDislikes($commentData['Dislikes']);
        }
        return $comment;
    }

    function getCommentByRef($commentidref) {
            $db = unserialize($_SESSION['db']);
            $comments = $db->getCommentsWithRef($commentidref);
            $commentObjects = array();
            foreach ($comments as $comment) {
                $commentObj = new Comment();
                $commentObj->setId($comment['ID_Comment']);
                $commentObj->setComment($comment['Comment']);
                $commentObj->setDate($comment['Date']);
                //start - set user, user reference of comment
                $userObj = $this->getUserByID($comment['ID_User']);
                $commentObj->setUser($userObj);
                if ($comment['ID_Comment_Reference']==null) {
                    $commentObj->setCommentReference(null);
                } else {
                    $commentObjRef = $this->getCommentByID($comment['ID_Comment_Reference']);
                    $commentObj->setCommentReference($commentObjRef);
                }
                //end - set user, user reference of comment
                $commentObj->setLikes($comment['Likes']);
                $commentObj->setDislikes($comment['Dislikes']);
                $commentObjects[] = $commentObj;
            }
            return $commentObjects;
    }
}
?>