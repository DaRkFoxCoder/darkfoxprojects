<?php 
class Project {
    private $id;
    private $name;
    private $description;
    private $lastUpdate;
    private $link;
    private $price;
    private $score;
    private $category;
    private $hot;
    private $comments = null;

    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return the name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return the description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return the link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return the price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return the lastUpdate
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }
    
    /**
     * @param lastUpdate
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
    }
    
    /**
     * @param link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @param price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return the score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return the category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getHot()
    {
        return $this->hot;
    }

    /**
     * @param hot
     */
    public function setHot($hot)
    {
        $this->hot = $hot;
    }

	/**
     * @return the comments
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    function __construct() {
		$this->setComments(array());
    }
}
?>