<?php
function __autoload($classname) {
	$fileBusinessLayer = $_SESSION['BASE_PATH']."/model/BusinessLayer/" . $classname . ".php";
	$filePersistanceLayer = $_SESSION['BASE_PATH']."/model/PersistanceLayer/" . $classname . ".php";
    if(file_exists($fileBusinessLayer)){
    	require_once($fileBusinessLayer);
    } else if(file_exists($filePersistanceLayer)) {
    	require_once($filePersistanceLayer);
    }
}
?>