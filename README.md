# README #

This project is a website for view projects, download and manage for admins.

### Tasks to do ###

* ~~Profile Page~~
* ~~Logout System~~
* ~~Comments System for Projects with Response Comment~~
* ~~Large description in show project page~~
* ~~Like/Dislike System 100%~~
* ~~Friendly Links~~
* ~~Similar Projects~~
* ~~Recent Projects~~
* ~~Hot Projects~~
* Perfil -> View Projects of User
* Multicategory in Projects
* Refresh Score in Rate Projects
* SEO

### Tasks to do [No important] ###
* About Page
* Contact Page
* Signup System
* Buy System
* Manage System for Admins [Alertifyjs for popups]