<!DOCTYPE html>
<html lang="en">
<?php
include ("../../controller/sessionController.php");
include ("../../controller/baseController.php");
$section['type'] = "profile";
$section['name'] = "Your Profile";
$section['description'] = "You can view your profile here";
include ("sections/head.php");
include ("sections/body.php");
?>
</html>