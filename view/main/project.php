<!DOCTYPE html>
<html lang="en">
<?php
include ("../../controller/sessionController.php");
include ("../../controller/baseController.php");
$projectObj = $projectsPage->getProject($_GET['id']);
$section['type'] = "project";
$section['name'] = $projectObj->getName();
$section['description'] = "Last Update: <font color='green'>".date("d-m-Y h:s", strtotime($projectObj->getLastUpdate()))."</font>";
include ("sections/head.php");
include ("sections/body.php");
if (isset($_GET['scroll']) && $_GET['scroll']) {
	?>
	<script>
	$('html, body').animate({
        scrollTop: $(".panel:eq(1)").offset().top-60
    }, 2000);
    $(".panel:eq(1)").effect( "highlight", {color:"#148739"}, 3000 );
    </script>
    <?php
}
?>
</html>