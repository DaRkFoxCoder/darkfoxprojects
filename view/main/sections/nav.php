<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand logo" href="./"></a>
		</div>
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="./projects/">Projects</a></li>
				<li><a href="./about/">About</a></li>
				<li><a href="./contact/">Contact</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<?php
				if (isset($_SESSION['user_session'])) {
					echo '
					<li><a href="./myprofile/"><span class="glyphicon glyphicon-user"></span> Welcome, '.$currentUser->getUsername().'</a></li>
					<li><a href="./logout/" class="cd-signout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
					';
				} else {
					echo '
					<li><a href="#signup" class="cd-signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
		      		<li><a href="#login" class="cd-signin"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
					';
				}
				?>
		    </ul>
		</div>
	</div>
</nav>
<?php include("login.php"); ?>