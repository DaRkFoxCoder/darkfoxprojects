<head>
<base href="<?php echo $settings['path'];?>" id="darkfoxprojects_base" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Page of DaRkFox Projects">
<meta name="author" content="DaRkFox">
<title><?php echo $namePage;?> - Home</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo $settings['pathCSS'];?>/bootstrap.min.css" rel="stylesheet">
<!-- CSS Alertify -->
<link rel="stylesheet" type="text/css" href="<?php echo $settings['pathCSS'];?>/alertify/alertify.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $settings['pathCSS'];?>/alertify/themes/bootstrap.min.css" />
<!-- CSS Comments of Projects -->
<link rel="stylesheet" type="text/css" href="<?php echo $settings['pathCSS'];?>/comments.css">
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<!-- Custom CSS -->
<link href="<?php echo $settings['pathCSS'];?>/3-col-portfolio.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $settings['pathCSS'];?>/loginForm.css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="<?php echo $settings['pathCSS'];?>/star-rating.min.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo $settings['pathCSS'];?>/jquery.contextMenu.min.css" media="all" rel="stylesheet" type="text/css"/>
<!-- CUSTOM CSS [NAVBAR] -->
<style>
	body {
    	font-family: Calibri !important;
	}
	.navbar-inverse {
		background-color: #148739;
	}
	.navbar-inverse .navbar-nav>li>a {
    	color: white;
	}
	.navbar-inverse .navbar-nav>li>a:hover {
    	color: #E62117;
		font-weight: bold;
	}
	.navbar-inverse .navbar-brand {
		color: white;
	}
	.navbar-inverse .navbar-brand:hover {
		color: #E62117;
		font-weight: bold;
	}
	.logo {
		background-image: url('<?php echo $settings['pathImages'];?>/logo.png');
		background-size: 100% 100%;
		background-repeat: no-repeat;
		width: 100px;
	}
	.logo:hover {
		background-image: url('<?php echo $settings['pathImages'];?>/logoHover.png');
		background-size: 100% 100%;
		background-repeat: no-repeat;
		width: 100px;
	}
</style>
</head>