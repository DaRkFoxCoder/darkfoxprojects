<!-- LOGIN BOX -->
<div class="cd-user-modal">
	<div class="cd-user-modal-container">
		<ul class="cd-switcher">
			<li><a href="#">Sign in</a></li>
			<li><a href="#">New account</a></li>
		</ul>
		<div id="cd-login">
			<form id="formLogin" class="cd-form" method="POST" action="">
				<p class="fieldset">
					<label class="image-replace cd-username" for="signin-email">Username</label>
					<input class="full-width has-padding has-border" id="uname" name="uname" type="text" placeholder="Username">
					<span class="cd-error-message">Invalid Username</span>
				</p>

				<p class="fieldset">
					<label class="image-replace cd-password" for="signin-password">Password</label>
					<input class="full-width has-padding has-border" id="upassword" type="text"  name="upassword" placeholder="Password">
					<a href="#" class="hide-password">Hide</a>
					<span class="cd-error-message">Invalid Password</span>
				</p>

				<p class="fieldset">
					<input type="checkbox" id="remember-me" checked>
					<label for="remember-me">Remember me</label>
				</p>

				<p class="fieldset">
					<input class="full-width" type="submit" value="Login">
				</p>
			</form>
			<!--<p class="cd-form-bottom-message"><a href="#">Forgot your password?</a></p>-->
		</div>
		<div id="cd-signup">
			<form class="cd-form">
				<p class="fieldset">
					<label class="image-replace cd-username" for="signup-username">Username</label>
					<input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username">
					<span class="cd-error-message">Error message here!</span>
				</p>

				<p class="fieldset">
					<label class="image-replace cd-email" for="signup-email">E-mail</label>
					<input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail">
					<span class="cd-error-message">Error message here!</span>
				</p>

				<p class="fieldset">
					<label class="image-replace cd-password" for="signup-password">Password</label>
					<input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Password">
					<a href="#" class="hide-password">Hide</a>
					<span class="cd-error-message">Error message here!</span>
				</p>

				<p class="fieldset">
					<input type="checkbox" id="accept-terms">
					<label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
				</p>

				<p class="fieldset">
					<input class="full-width has-padding" type="submit" value="Create account">
				</p>
			</form>
		</div>
		<div id="cd-reset-password">
			<p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

			<form class="cd-form">
				<p class="fieldset">
					<label class="image-replace cd-email" for="reset-email">E-mail</label>
					<input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
					<span class="cd-error-message">Error message here!</span>
				</p>

				<p class="fieldset">
					<input class="full-width has-padding" type="submit" value="Reset password">
				</p>
			</form>

			<p class="cd-form-bottom-message"><a href="#">Back to log-in</a></p>
		</div>
		<a href="#" class="cd-close-form">Close</a>
	</div>
</div>