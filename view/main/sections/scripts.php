<!-- jQuery -->
<script src="<?php echo $settings['pathJS'];?>/jquery.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js" type="text/javascript"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo $settings['pathJS'];?>/bootstrap.min.js"></script>
<!-- Rating System JQuery -->
<script src="<?php echo $settings['pathJS'];?>/star-rating.min.js" type="text/javascript"></script>
<!-- Other Scripts	-->
<script src="<?php echo $settings['pathJS'];?>/alertify.min.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/modernizr.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/ratingSystem.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/loginSystem.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/dropzone.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/uploader.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/comments.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/jquery.contextMenu.min.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/jqueryValidation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo $settings['pathJS'];?>/jqueryValidation/additional-methods.min.js" type="text/javascript"></script>
<!--<script src="<?php echo $settings['pathJS'];?>/jqueryValidation/localization/messages_es.js" type="text/javascript"></script>-->
<script>
$(document).ready(function() {
	$("#newComment").validate({
		//lang: 'es',
		rules: {
			comment: {
				required: true,
				minlength: 6,
			}
		}
	});
});
</script>
<script src="<?php echo $settings['pathJS'];?>/general.js" type="text/javascript"></script>
<script>
alertify.YoutubeDialog || alertify.dialog('YoutubeDialog',function(){
    var iframe;
    return {
        // dialog constructor function, this will be called when the user calls alertify.YoutubeDialog(videoId)
        main:function(videoId, title){
            //set the videoId setting and return current instance for chaining.
            return this.set({ 
                'videoId': videoId,
                'title': title
            });
        },
        // we only want to override two options (padding and overflow).
        setup:function(){
            return {
                options:{
                    //disable both padding and overflow control.
                    padding : !1,
                    overflow: !1,
                }
            };
        },
        // This will be called once the DOM is ready and will never be invoked again.
        // Here we create the iframe to embed the video.
        build:function(){           
            // create the iframe element
            iframe = document.createElement('iframe');
            iframe.frameBorder = "no";
            iframe.width = "100%";
            iframe.height = "100%";
            // add it to the dialog
            this.elements.content.appendChild(iframe);

            //give the dialog initial height (half the screen height).
            this.elements.body.style.minHeight = screen.height * .5 + 'px';
			var errorHeader = '<span class="fa fa-times-circle fa-2x" '
                +    'style="vertical-align:middle;color:#e10000;">'
                + '</span> ' + this.title;
            this.setHeader(errorHeader);
        },
        // dialog custom settings
        settings:{
            videoId:undefined
        },
        // listen and respond to changes in dialog settings.
        settingUpdated:function(key, oldValue, newValue){
            switch(key){
               case 'videoId':
                    iframe.src = "https://www.youtube.com/embed/" + newValue + "?enablejsapi=1";
                break;   
            }
        },
        // listen to internal dialog events.
        hooks:{
            // triggered when the dialog is closed, this is seperate from user defined onclose
            onclose: function(){
                iframe.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}','*');
            },
            // triggered when a dialog option gets update.
            // warning! this will not be triggered for settings updates.
            onupdate: function(option,oldValue, newValue){
                switch(option){
                    case 'resizable':
                        if(newValue){
                            this.elements.content.removeAttribute('style');
                            iframe && iframe.removeAttribute('style');
                        }else{
                            this.elements.content.style.minHeight = 'inherit';
                            iframe && (iframe.style.minHeight = 'inherit');
                        }
                    break;    
                }    
            }
        }
    };
});
alertify.dialog('IframeDialog',function(){
    var iframe;
    return {
        main:function(title, srciframe){
            this.title = title;
            this.srciframe = srciframe;
			if (arguments.length >= 2) {
				this.class = arguments[2];
			}
        },
        build:function(){
            iframe = document.createElement('iframe');
            iframe.frameBorder = "no";
            iframe.width = "100%";
            iframe.height = "100%";
            this.elements.content.appendChild(iframe);
            this.elements.body.style.minHeight = screen.height * .5 + 'px';
        },
		prepare:function(){
			var classIframe = "fa fa-times-circle fa-2x";
			if (typeof this.class != "undefined") {
				classIframe = this.class;
			}
			var errorHeader = '<span class="'+classIframe+'" '
                +    'style="vertical-align:middle;color:#e10000;">'
                + '</span> ' + this.title;
            this.setHeader(errorHeader);
			iframe.src = this.srciframe;
      	}
    };
});
//alertify.YoutubeDialog('GODhPuM5cEE', 'AlertifyJS Video').set({frameless:false});
</script>