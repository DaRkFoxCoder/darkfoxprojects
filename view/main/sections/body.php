<body>
	<!-- USER ID LOGGED -->
    <?php include("../../controller/userController.php"); ?>
	<span id="iduser" style="display:none"><?php if (isset($_SESSION['user_session'])) { echo $_SESSION['user_session']; } ?></span>
	<!-- Navigation -->
	<?php include("sections/nav.php"); ?>
	<!-- Page Content -->
	<div class="container-fluid">
		<!-- Page Header -->
		<div class="row">
			<div class="col-md-offset-1 col-md-10">
				<h1 class="page-header">
					<?php if (isset($section)) { echo $section['name']; ?> <small><?php echo $section['description']; } ?></small>
				</h1>
				<?php
				if (isset($section) && $section['type']=="index") {
					include_once("showProjects.php");
					showProjects($projectsPage->getHotProjects(), 4, true);
					echo '
					<div class="col-md-12" style="padding:0">
						<h1 class="page-header">
							Last Projects
							<small>You can found last projects here</small>
						</h1>
					';
					showProjects($projectsPage->getProjects(), 4);
					echo '</div>';
				}
				?>
			</div>
		</div>
		<!-- Projects Row -->
		<div class="row">
				<div class="col-md-offset-1 col-md-10">
				<?php
				if (isset($section) && !empty($section)) {
					switch($section['type']) {
						case "index": {
							//nothing
							break;
						}
						case "project": {
							include("showProject.php");
							include("showProjects.php");
							showProject($projectsPage->getProject($_GET['id']));
							$projectCategory = $projectsPage->getProject($_GET['id'])->getCategory();
							echo '
							<h1 class="page-header">
								Similar Projects
							</h1>
							';
							showProjects($projectsPage->getProjectsWithCat($projectCategory, $_GET['id']));
							break;
						}
						case "projects": {
							include("showProjects.php");
							showProjects($projectsPage->getProjects());
							break;
						}
						case "profile": {
							include("showProfile.php");
							showProfile();
							break;
						}
						default: {
							echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span class='sr-only'>Error:</span>&nbsp;ERROR 404. Page not Available!</div>";
							break;
						}
					}
				} else {
					echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span class='sr-only'>Error:</span>&nbsp;ERROR 404. Page not Available!</div>";
							break;
				}
				?>
			</div>
		</div>
		<!--<hr>-->
		<!-- Pagination -->
		<!--
		<div class="row text-center">
			<div class="col-lg-12">
				<ul class="pagination">
					<li><a href="#">&laquo;</a></li>
					<li class="active"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&raquo;</a></li>
				</ul>
			</div>
		</div>
		-->
		<hr>
		<!-- Footer -->
		<?php include("sections/footer.php"); ?>
	</div>
	<!-- SCRIPTS -->
	<?php include("sections/scripts.php"); ?>
</body>