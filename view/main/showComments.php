<?php
include("../../controller/utilitiesController.php");
function showCommentDiv($comment, $commentOwner) {
    global $projectsPage;
    $diffDate = getDateDiff($comment->getDate());
    ?>
    <div class="col-md-12">
        <div class="panel panel-white post panel-shadow">
            <div class="post-heading">
                <div class="pull-left image">
                    <?php
                    //path for file_exists is different. Important!
                    $imageAvatar = "images/avatars/".$comment->getUser()->getId()."/avatar.png";
                    if (!file_exists($imageAvatar)) {
                        $imageAvatar = "view/main/images/avatars/avatar.png";
                    } else {
                        $imageAvatar = "view/main/images/avatars/".$comment->getUser()->getId()."/avatar.png";
                    }
                    ?>
                    <span id="<?php echo $comment->getId(); ?>" " class="commentID" style="display: none;"></span>
                    <span id="<?php echo $comment->getUser()->getUsername();?>" class="usernameReply" style="display: none;"></span>
                    <img id=<?php echo $comment->getUser()->getId();?>  src="<?php echo $imageAvatar;?>" class="img-circle avatar" alt="user profile image">
                </div>
                <div class="pull-left meta">
                    <div class="title h5">
                        <a href="./profile/<?php echo $comment->getUser()->getId();?>"><b><?php echo $comment->getUser()->getUsername();?></b></a>
                        <?php
                        if (isset($commentOwner) && $commentOwner != null) {
                            $idOwnerComment = $commentOwner->getUser()->getId();
                            $usernameOwnerComment = $commentOwner->getUser()->getUsername();
                            $RespUserNameLink = '<a href="profile.php?user='.$idOwnerComment.'"><b>'.$usernameOwnerComment.'</b></a>';
                            echo "has responsed ".$RespUserNameLink;
                        } else {
                            echo "has comment.";
                        }
                        ?>
                    </div>
                    <h6 class="text-muted time"><?php echo $diffDate;?> ago</h6>
                </div>
            </div> 
            <div class="post-description">
            	<div class="col-md-12 col-xs-12" style="word-wrap: break-word">
					<?php
                    if (isset($commentOwner) && $commentOwner != null) {
                        $commentText = $commentOwner->getComment();
                        if( strlen( $commentOwner->getComment()) > 80) {
                            $commentText = explode( "\n", wordwrap( $commentOwner->getComment(), 80));
                            $commentText = $commentText[0] . '...';
                        }
                        echo '
                        <blockquote>
                            <p>'.$commentText.'</p>
                            <small>Commented by <cite title="'.$commentOwner->getUser()->getUsername().'">'.$commentOwner->getUser()->getUsername().'</cite></small>
                        </blockquote>';
                    }
                    ?>
                </div>
                <div class="col-md-12 col-xs-12" style="word-wrap: break-word">
					<?php echo "<p>".$comment->getComment()."</p>"; ?>
                </div>
                <div class="stats">
                    <a data-toggle="tooltip" title="Like!" href="#" class="btn btn-default stat-item likeBtn">
                        <i class="fa fa-thumbs-up icon"></i><?php echo $comment->getLikes();?>
                    </a>
                    <a data-toggle="tooltip" title="Dislike!" href="#" class="btn btn-default stat-item dislikeBtn">
                        <i class="fa fa-thumbs-down icon"></i><?php echo $comment->getDislikes();?>
                    </a>
                    <a data-toggle="tooltip" title="Reply" href="#" class="btn btn-default stat-item pull-right hidden-xs replyBtn">
                    	<i class="fa fa-reply icon"></i>Reply
                    </a>
                    <a data-toggle="tooltip" title="Reply" href="#" class="btn btn-default stat-item pull-right visible-xs replyBtn">
                    	<i class="fa fa-reply icon"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php
    $commentsReference = $projectsPage->getCommentByRef($comment->getId());
    foreach ($commentsReference as $commentReference) {
        showCommentDiv($commentReference, $comment);
    }
}
?>
<div class="container">
    <div class="row">
        <!-- START - FORM NEW COMMENT -->
        <div class="col-md-12">
            <div class="panel panel-white post panel-shadow status-upload" style="padding:0;">
                    <form id="newComment" action="" method="POST">
                        <?php
                        if (!isset($_SESSION['user_session'])) {
                            echo '<textarea disabled name="comment" placeholder="You not are logged." ></textarea>';
                            echo '
                            <button data-toggle="tooltip" disabled title="Send your comment" type="submit" class="btn btn-success green" style="margin-bottom: 4px;">
                                <span class="glyphicon glyphicon-send"></span>&nbsp;Comment
                            </button>';
                            echo '
                            <a style="margin-left: 5px; margin-top: 5px;" id="userReplyRef" data-toggle="tooltip" title="" class="btn btn-default">
                                <i class="glyphicon glyphicon-question-sign"></i>&nbsp;Answer for this project.
                            </a>
                            ';
                        } else {
                            echo '<textarea name="comment" placeholder="Reply here..." ></textarea>';
                            echo '
                            <button data-toggle="tooltip" title="Send your comment" type="submit" class="btn btn-success green" style="margin-bottom: 4px;">
                                <span class="glyphicon glyphicon-send"></span>&nbsp;Comment
                            </button>';
                            echo '
                            <a style="margin-left: 5px; margin-top: 5px;" id="userReplyRef" data-toggle="tooltip" title="" class="btn btn-default">
                                <i class="glyphicon glyphicon-question-sign"></i>&nbsp;Answer for this project.
                            </a>';
                        }
                        ?>
                        <!--<ul>
                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Audio"><i class="fa fa-music"></i></a></li>
                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Video"><i class="fa fa-video-camera"></i></a></li>
                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Sound Record"><i class="fa fa-microphone"></i></a></li>
                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture"><i class="fa fa-picture-o"></i></a></li>
                        </ul>-->
                    </form>
            </div>
        </div>
        <!-- END - FORM NEW COMMENT-->
    	<?php
        $comments = $project->getComments();
        if (count($comments)==0) {
            echo '
            <div class="col-md-12 alert alert-info" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>Any comment. You can be the first to comment!
            </div>';
        }
		foreach ($comments as $comment) {
			showCommentDiv($comment, false);
		}
		?>
    </div>
</div>