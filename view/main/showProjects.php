<?php 

function showProjects($projects = null, $limit = 0) {
    $countProject = 0;
    foreach ($projects as $project) {
    	if ($limit != 0) {
    		if ($countProject<$limit) {
    			printProjectHTML($project);
    		}
    	} else {
    		printProjectHTML($project);
    	}
        $countProject++;
    }
}

function printProjectHTML($project) {
    global $projectsPage, $currentUser, $settings;
    //path for file_exists is different. Important!
    $pathImage = "images/projects/".$project->getId().".png";
    $relativePathImage = "";
    if (!file_exists($pathImage)) {
        $pathImage = "view/main/images/projects/noimage.png";
    	$relativePathImage = $settings['pathImages']."/projects/noimage.png";
    } else {
        $pathImage = "view/main/images/projects/".$project->getId().".png";
    	$relativePathImage = $settings['pathImages']."/projects/".$project->getId().".png";
    }
    ?>
    <div class="col-md-3 portfolio-item project" id="project_<?php echo $project->getId(); ?>">
		<a href="<?php echo "./project/".$project->getId();?>"> <img class="img-responsive"
			src="<?php echo $relativePathImage;?>" alt="<?php echo $project->getName();?>">
		</a>
		<h3>
			<a href="<?php echo "./project/".$project->getId();?>"><?php echo $project->getName();?></a>
		</h3>
		<p style="overflow: hidden; height: 24px">
		<?php
		$description = $project->getDescription();
		$limit = 56;
		if (strlen($description) > $limit) {
			$description = substr($description,0,$limit) . "..."; 
		}
        echo $description;
		?>
        </p>
        <style>
		.css-title:before {
			text-align: center;
			content: "Administration Panel";
			display: block;
			background: #DDD;
			padding: 2px;
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 11px;
			font-weight: bold;
		}
		</style>
        <?php
		if (isset($_SESSION['user_session']) && $currentUser->getStatus() == 1) {
			?>
			<script>
			function loadContextMenu()  {
				$.contextMenu({
					selector: '.portfolio-item',
					className: 'css-title',
					callback: function(key, options) {
						switch(key) {
							case "addProject": {
								//add iframe dialog
								break;
							}
							case "editProject": {
								alertify.IframeDialog('Edit Project <?php echo $project->getName();?>', 'admin.php?edit=project&id=<?php echo $project->getId();?>').set({frameless:false});
								break;
							}
							case "deleteProject": {
								//delete ajax here
								break;
							}
						}
					},
					items: {
						"addProject": {name: "Add Project", icon: "add"},
						"editProject": {name: "Edit Project", icon: "edit"},
						"removeProject": {name: "Remove Project", icon: "delete"},
						"quit": {
							name: "Quit", icon: function(){return 'context-menu-icon context-menu-icon-quit';}
						}
					}
				});
		
				$('.context-menu-one').on('click', function(e){
					console.log('clicked', this);
				})
			}
			window.onload = loadContextMenu;
			</script>
            <?php
			//echo '<span style="color:green;" class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Change Description"></span>';
		}
		?>
        <input class="rate" value="<?php echo $project->getScore();?>">
        <p><?php echo "Category: ".$projectsPage->getCategory($project->getCategory())->getName();?></p>
        <p>
        <?php
        $price = $project->getPrice();
        if ($price<=0) {
            echo "Price: <strong style='color:green'>FREE</strong>";
        } else {
            echo "Price: <strong style='color:red'>".$price." €</strong>";
        }
        ?>
        </p>
	</div>
    <?php
}

?>