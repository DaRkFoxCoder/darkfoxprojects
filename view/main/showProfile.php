<?php 
function showProfile() {
	global $userid, $currentUser, $projectsPage, $pathCSS;
    if (isset($_GET['user'])) {
        $currentUser = $projectsPage->getUserByID($_GET['user']);
    }
	$isMyProfile = false;
	if ($userid == $currentUser->getId()) {
		$isMyProfile = true;
	}
    if ($currentUser!=null) {
    ?>
    <div class="col-md-12">
        <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                        	<?php
                            if (file_exists("images/avatars/".$currentUser->getId()."/avatar.png")) {
								$imageSRC = "./view/main/images/avatars/".$currentUser->getId()."/avatar.png";
							} else {
								$imageSRC = "./view/main/images/avatars/avatar.png";
							}
							if ($isMyProfile) {
								?>
                                <span id="avatarUploader">
                            		<img id="avatar" src="<?php echo $imageSRC; ?>" width="380" height="500" alt="Avatar of <?php echo $currentUser->getUsername(); ?>" data-toggle="tooltip" title="Click for change Avatar!" class="img-rounded img-responsive" />
                                </span>
                                <?php
							} else {
								?>
                                <span>
                            		<img src="<?php echo $imageSRC; ?>" width="380" height="500" alt="Avatar of <?php echo $currentUser->getUsername(); ?>" data-toggle="tooltip" title="Avatar of <?php echo $currentUser->getUsername(); ?>!" class="img-rounded img-responsive" />
                                </span>
                                <?php
							}
							?>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <h4><?php echo $currentUser->getUsername(); ?></h4>
                            <h4>
							<?php
                            switch($currentUser->getStatus()) {
								case 0: {
									echo "<font color='#10AF40'>User</font>";
									break;
								}
								case 1: {
									echo "<font color='#C90B0B'>Administrator</font>";
									break;
								}
							}
							?>
                            </h4>
                            <p>
                            <i class="glyphicon glyphicon-envelope"></i><?php if (!empty($currentUser->getEmail())) { echo $currentUser->getEmail(); } else { echo "None"; } ?>
                            </p>
                            <i class="glyphicon glyphicon-globe"></i><a href="<?php echo $currentUser->getWebsite();?>"><?php echo $currentUser->getWebsite();?></a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <style>
	.glyphicon {
		margin-bottom: 10px;margin-right: 10px;
	}
	</style>
    <?php
    } else {
        echo '
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>Profile not exists!
        </div>';
    }
}
?>