var replyid = null, commentid;
var cacheVal = "none";
$(".replyBtn").click(function(e) {
	e.preventDefault();
	replyid = $(this).parent().parent().parent().find(".post-heading img").attr("id");
	commentid = $(this).parent().parent().parent().find(".post-heading .commentID").attr("id");
	replyusername = $(this).parent().parent().parent().find(".post-heading .usernameReply").attr("id");
	$("#userReplyRef").css("display", "inline-block");
	$("#userReplyRef").html('<i class="glyphicon glyphicon-question-sign"></i>&nbsp;Answer to ' + replyusername);

	$('html, body').animate({
        scrollTop: $("#newComment").offset().top-80
    }, 2000);
    $("#newComment").effect( "highlight", {color:"#148739"}, 3000 );
});
$("#newComment").submit(function(e) {
	e.preventDefault();
	if($("#newComment").valid()) {
		var comment = $(this).find("textarea:eq(0)").val();
		var userID = $("#iduser").text();
		var projectID = $("#idproject").text();
		if (userID!="") {
			var request = $.ajax({
			  url: "./controller/newCommentController.php",
			  method: "POST",
			  data: { comment : comment, userid : userID , projectid : projectID, commentidref: commentid },
			  dataType: "json"
			});
			request.done(function(response) {
				alertify.success("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> You comment is sended successfully.");
				window.setTimeout(function () {
					location.href = location.href.replace("/scroll/", "") + "/scroll/";	//if already have /scroll/ in url => replace
				}, 1000);
			});
		} else {
			$(this).find("textarea:eq(0)").attr("placeholder", "You need login in your account for comment...");
		}
	}
});
$(".likeBtn").click(function(e) {
	e.preventDefault();
	var commentid = $(this).parent().parent().parent().find(".post-heading .commentID").attr("id");
	var userid = $("#iduser").text();
	if (userid!="") {
		if (cacheVal == "like" || cacheVal == "none") {
			valorateComment(commentid, userid, "like", this);
		} else {
			alertify.error("<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span> Don't like comment because you already dislike this comment.");
		}
	} else {
		alertify.error("<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span> You need login in your account for valorate comment.");
	}
});
$(".dislikeBtn").click(function(e) {
	e.preventDefault();
	var commentid = $(this).parent().parent().parent().find(".post-heading .commentID").attr("id");
	var userid = $("#iduser").text();
	if (userid!="") {
		if (cacheVal == "dislike" || cacheVal == "none") {
		valorateComment(commentid, userid, "dislike", this);
		} else {
			alertify.error("<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span> Don't dislike comment because you already like this comment.");
		}
	} else {
		alertify.error("<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span> You need login in your account for valorate comment.");
	}
});
function valorateComment(commentid, userid, type, element) {
	var type = type;
	var request = $.ajax({
		  url: "./controller/valorateCommentController.php",
		  method: "POST",
		  data: { commentid : commentid , userid : userid, type: type },
		  dataType: "json"
		});
	request.done(function(response) {
		$('.tooltip').remove();
		$(element).parent(".stats").find(".stat-item").each(function( index, element ) {
			$(element).css("color", "");
		});
		switch(response.status) {
			case "valorated": {
				$(element).css("color", "#337ab7!important");
				var htmlIcon = $(element).find("i")[0].outerHTML;
				var totalVal = parseInt($(element).text()) + 1;
				$(element).html(htmlIcon+totalVal);
				alertify.success("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> You valorated a comment successfully.");
				cacheVal = type;
				break;
			}
			case "unvalorated": {
				$(element).css("color", "#337ab7!important");
				var htmlIcon = $(element).find("i")[0].outerHTML;
				var totalVal = parseInt($(element).text()) - 1;
				$(element).html(htmlIcon+totalVal);
				alertify.success("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> You valorated a comment successfully.");
				break;
			}
			case "already_valorated": {
				alertify.error("<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span> Already valorated this comment.");
				break;
			}
			case "fail": {
				alertify.error("<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span> Error in valoration process.");
				break;
			}
		}
	});
}
