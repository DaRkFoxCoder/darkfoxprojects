function loginRequest() {
	var uname = $("#uname").val();
	var upassword = $("#upassword").val();
	var request = $.ajax({
	  url: "./controller/loginController.php",
	  method: "POST",
	  data: { uname : uname, upassword : upassword },
	  dataType: "json"
	});
	 
	request.done(function(response) {
		if (response.logged) {
			$(".cd-user-modal").removeClass("is-visible");
			alertify.success("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> You have logged in account. You are redirected in 3 seconds.");
			window.setTimeout(function () {
		        location.href = "index.php";
		    }, 3000);
		} else {
			$("#cd-login").find('span:eq(0)').attr('class', 'cd-error-message');
			$("#cd-login").find('span:eq(1)').attr('class', 'cd-error-message');
			switch(response.invalid) {
				case "username": {
					$("#cd-login #uname").addClass('has-error').parent().find("span:eq(0)").toggleClass('is-visible');
					break;
				}
				case "password": {
					$("#cd-login #uname").removeClass('has-error');
					$("#cd-login #upassword").addClass('has-error').parent().find("span:eq(0)").toggleClass('is-visible');
					break;
				}
			}
		}
	});
	 
	request.fail(function( jqXHR, textStatus ) {
		console.log( "Request failed: " + textStatus );
	});
}
$(document).ready(function() {
	$("#formLogin").submit(function(e) {
		e.preventDefault();
		loginRequest();
	});
	jQuery(document).ready(function($){
	var formModal = $('.cd-user-modal'),
		formLogin = formModal.find('#cd-login'),
		formSignup = formModal.find('#cd-signup'),
		formForgotPassword = formModal.find('#cd-reset-password'),
		formModalTab = $('.cd-switcher'),
		tabLogin = formModalTab.children('li').eq(0).children('a'),
		tabSignup = formModalTab.children('li').eq(1).children('a'),
		forgotPasswordLink = formLogin.find('.cd-form-bottom-message a'),
		backToLoginLink = formForgotPassword.find('.cd-form-bottom-message a'),
		mainNav = $('.navbar');

	mainNav.on('click', function(event){
		$(event.target).is(mainNav) && mainNav.children('ul').toggleClass('is-visible');
	});

	mainNav.on('click', '.cd-signup', signup_selected);
	mainNav.on('click', '.cd-signin', login_selected);

	formModal.on('click', function(event){
		if( $(event.target).is(formModal) || $(event.target).is('.cd-close-form') ) {
			formModal.removeClass('is-visible');
		}	
	});
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		formModal.removeClass('is-visible');
	    }
    });

	formModalTab.on('click', function(event) {
		event.preventDefault();
		( $(event.target).is( tabLogin ) ) ? login_selected() : signup_selected();
	});

	$('.hide-password').on('click', function(){
		var togglePass= $(this),
			passwordField = togglePass.prev('input');
		
		( 'password' == passwordField.attr('type') ) ? passwordField.attr('type', 'text') : passwordField.attr('type', 'password');
		( 'Hide' == togglePass.text() ) ? togglePass.text('Show') : togglePass.text('Hide');
		passwordField.putCursorAtEnd();
	});

	forgotPasswordLink.on('click', function(event){
		event.preventDefault();
		forgot_password_selected();
	});

	backToLoginLink.on('click', function(event){
		event.preventDefault();
		login_selected();
	});

	function login_selected(){
		mainNav.children('ul').removeClass('is-visible');
		formModal.addClass('is-visible');
		formLogin.addClass('is-selected');
		formSignup.removeClass('is-selected');
		formForgotPassword.removeClass('is-selected');
		tabLogin.addClass('selected');
		tabSignup.removeClass('selected');
	}

	function signup_selected(){
		mainNav.children('ul').removeClass('is-visible');
		formModal.addClass('is-visible');
		formLogin.removeClass('is-selected');
		formSignup.addClass('is-selected');
		formForgotPassword.removeClass('is-selected');
		tabLogin.removeClass('selected');
		tabSignup.addClass('selected');
	}

	function forgot_password_selected(){
		formLogin.removeClass('is-selected');
		formSignup.removeClass('is-selected');
		formForgotPassword.addClass('is-selected');
	}

	if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
		  	}
		}).blur(function() {
		 	var input = $(this);
		  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.val(input.attr('placeholder'));
		  	}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
		  	$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
			 		input.val('');
				}
		  	})
		});
	}

});


jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {
    	if (this.setSelectionRange) {
      		var len = $(this).val().length * 2;
      		this.focus();
      		this.setSelectionRange(len, len);
    	} else {
      		$(this).val($(this).val());
    	}
	});
};
	 $(".loginBtn").click(function(){
		 event.preventDefault();
		 
	 });
});