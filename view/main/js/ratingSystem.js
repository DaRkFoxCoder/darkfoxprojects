$('.rate').rating({
	min: 0,
	max: 5,
	step: 0.5,
	size: 'xs',
	showClear: false,
	'starCaptions': {0:'Very Bad', 0.5:'Very Bad', 1:'Very Bad', 1.5:'Bad', 2:'Bad', 2.5: 'Normal', 3:'Normal', 3.5:'Good', 4:'Good', 4.5:'Excelent', 5:'Perfect'}
});
$('.rate').on('rating.change', function() {
	var rate = $(this).val();
	var idproject = $(this).closest(".project").attr("id").split('_')[1];
	var iduser = $("#iduser").text();
	var request = $.ajax({
	  url: "./controller/rateController.php",
	  method: "POST",
	  data: { idproject : idproject, iduser : iduser, rate : rate },
	  dataType: "json"
	});
	 
	request.done(function(response) {
		if (response.rate) {
			alertify.success("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> You have voted " + rate + " in project!");
		} else {
			if (response.msg == "ALREADY_VOTED") {
				alertify.error("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>&nbsp;Already Voted in this project.");
			} else if (response.msg == "QUERY_ERROR") {
				alertify.error("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>&nbsp;Error in process.");
			} else if (response.msg == "NO_LOGGED") {
				alertify.error("<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>&nbsp;No puedes votar si no estas logeado en una cuenta.");
			}
		}
	});
});