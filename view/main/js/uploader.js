Dropzone.autoDiscover = false;
var maxImageWidth = 380, maxImageHeight = 500;
var userid = $("#iduser").text();
var dropzone = $("#avatar").dropzone({
	url: "./controller/uploadController.php?id="+userid, maxFilesize: 1, acceptedFiles: 'image/png',
	createImageThumbnails: true,
	thumbnailWidth: maxImageWidth,
    thumbnailHeight: maxImageHeight,
	previewsContainer: null,
	addRemoveLinks: true,
	init: function()
	{
		this.on("error", function(file){if (!file.accepted) alert("Solo se permite imagenes PNG de 380 x 500 pixeles");});
		this.on("thumbnail", function(file) {
			// The thumbnail has been generated, check if the accept function already provided an _accept function:
			if (file._accept) { file._accept(); }
		});
	},
	accept: function(file, done) {
	  // If image dimensions exist: call done right away with an error or without.
	  // otherwise set a function that the 'thumbnail' event handler can call.
	  file._accept = function() {
		if (file.width != maxImageWidth || file.height != maxImageHeight) {
			done("Invalid dimensions!");
		} else {
			var elementPreview = document.createElement("b");
			elementPreview.innerHTML = file.previewTemplate.outerHTML;
			$("#avatar").attr("src", $(elementPreview).find("img").attr("src"));
			done(); // Call it with an error if you want to reject the file.
		}
	  };
	},
});