<?php 
function showProject($project = null) {
    global $projectsPage, $settings;
    //path for file_exists is different. Important!
    $pathImage = "images/projects/".$project->getId().".png";
    $relativePathImage = "";
    if (!file_exists($pathImage)) {
        $pathImage = "view/main/images/projects/noimage.png";
        $relativePathImage = $settings['pathImages']."/projects/noimage.png";
    } else {
        $pathImage = "view/main/images/projects/".$project->getId().".png";
        $relativePathImage = $settings['pathImages']."/projects/".$project->getId().".png";
    }
    $price = $project->getPrice();
    $btnBuy = "ERROR";
    if ($price<=0) {
         $btnBuy = "<a target='_blank' href=".$project->getLink().">Download</a>";
    } else {
        $btnBuy = "<a href='#'>Buy</a>";
    }
    ?>
    <div style="border: 1px solid green;" class="col-md-12 panel panel-white panel-shadow project" id="project_<?php echo $project->getId(); ?>">
    	<span id="idproject" style="display: none"><?php echo $project->getId(); ?></span>
        <div class="col-md-4">
        <a href="<?php echo "./project/".$project->getId();?>"> <img class="img-responsive"
            src="<?php echo $pathImage;?>" alt="<?php echo $project->getName();?>">
        </a>
        </div>
        <div class="col-md-8">
        <h3>
            <?php echo $btnBuy; ?>
        </h3>
        <p><?php echo $project->getDescription();?></p>
        <input class="rate" value="<?php echo $project->getScore();?>">
        <p><?php echo "Category: ".$projectsPage->getCategory($project->getCategory())->getName();?></p>
        <?php
        if ($price<=0) {
            echo "Price: <strong style='color:green'>FREE</strong>";
        } else {
            echo "Price: <strong style='color:red'>".$price." €</strong>";
        }
        ?>
        </div>
    </div>
    <?php
	include("showComments.php");
}
?>